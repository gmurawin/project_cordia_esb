package com.enxoo.restcall;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;

import org.joda.time.LocalDateTime;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.mule.util.store.SimpleMemoryObjectStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;


public class RestAPICall  {
	
	@Autowired
	private SimpleMemoryObjectStore memoryStore = null;
	
	private String USERNAME;
	private String PASSWORD;
	private String LOGINURL;
	private String CLIENTID;
	private String CLIENTSECRET;
    
	public Object getValidAccessToken() throws Exception {
		if(memoryStore.contains("access_token")){
			if(memoryStore.contains("token_valid_to")){
				LocalDateTime newDate = new LocalDateTime();
				LocalDateTime tokenValidTo = (LocalDateTime) memoryStore.retrieve("token_valid_to");
//				DateTimeFormatter format = DateTimeFormat.forPattern("yyyy/mm/dd HH:mm:ss");
//				DateTime time = format.parseDateTime(tokenValidTo);
				if(newDate.isAfter(tokenValidTo)){
					memoryStore.remove("access_token");
					memoryStore.remove("token_valid_to");
					return onCall();
				}
				else
					return memoryStore.retrieve("access_token");
			}
			memoryStore.remove("access_token");
			return onCall();
		}
		return onCall();
	}
	
	private Object onCall() throws Exception {
	    
	    final String GRANTSERVICE = "/services/oauth2/token?grant_type=password";
	    
	    PoolingHttpClientConnectionManager clientConnectionManager = new PoolingHttpClientConnectionManager(getRegistry());
        clientConnectionManager.setMaxTotal(100);
        clientConnectionManager.setDefaultMaxPerRoute(20);
	    HttpClient httpclient = HttpClientBuilder.create().setConnectionManager(clientConnectionManager).build();
	    
        // Assemble the login request URL
        String loginURL = LOGINURL +
                          GRANTSERVICE +
                          "&client_id=" + CLIENTID +
                          "&client_secret=" + CLIENTSECRET +
                          "&username=" + USERNAME +
                          "&password=" + PASSWORD;
        
        System.out.println("LOGIN URL: " + loginURL);
 
        // Login requests must be POSTs
        HttpPost httpPost = new HttpPost(loginURL);
        HttpResponse response = null;
 
        try {
            // Execute the login POST request
            response = httpclient.execute(httpPost);
        } catch (ClientProtocolException cpException) {
            cpException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
 
        // verify response is HTTP OK
        final int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            System.out.println("Error authenticating to Force.com: "+statusCode + "resp:" + response.getEntity() + "head: " + response.getAllHeaders().toString());
            // Error is in EntityUtils.toString(response.getEntity())
            return EntityUtils.toString(response.getEntity());
        }
 
        String getResult = null;
        try {
            getResult = EntityUtils.toString(response.getEntity());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        JSONObject jsonObject = null;
        String loginAccessToken = null;
        String loginInstanceUrl = null;
        try {
            jsonObject = (JSONObject) new JSONTokener(getResult).nextValue();
            loginAccessToken = jsonObject.getString("access_token");
            loginInstanceUrl = jsonObject.getString("instance_url");
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
        System.out.println(response.getStatusLine());
        System.out.println("Successful login");
        System.out.println("  instance URL: "+loginInstanceUrl);
        System.out.println("  access token/session ID: "+loginAccessToken);
 
        // release connection
        httpPost.releaseConnection();
        memoryStore.store("access_token", loginAccessToken);
        LocalDateTime ldt = new LocalDateTime().plusHours(2);
        memoryStore.store("token_valid_to", ldt);
        System.out.println(memoryStore.retrieve("token_valid_to"));
		return loginAccessToken;
    }

	public SimpleMemoryObjectStore getMemoryStore() {
		return memoryStore;
	}

	public void setMemoryStore(SimpleMemoryObjectStore memoryStore) {
		this.memoryStore = memoryStore;
	}
	
	private static Registry<ConnectionSocketFactory> getRegistry() throws KeyManagementException, NoSuchAlgorithmException {
	    SSLContext sslContext = SSLContexts.custom().build();
	    SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
	            new String[]{"TLSv1.2"}, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
	    return RegistryBuilder.<ConnectionSocketFactory>create()
	            .register("https", sslConnectionSocketFactory)
	            .build();
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public String getLOGINURL() {
		return LOGINURL;
	}

	public void setLOGINURL(String lOGINURL) {
		LOGINURL = lOGINURL;
	}

	public String getCLIENTID() {
		return CLIENTID;
	}

	public void setCLIENTID(String cLIENTID) {
		CLIENTID = cLIENTID;
	}

	public String getCLIENTSECRET() {
		return CLIENTSECRET;
	}

	public void setCLIENTSECRET(String cLIENTSECRET) {
		CLIENTSECRET = cLIENTSECRET;
	}
	
	
}