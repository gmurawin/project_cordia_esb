package com.enxoo.resource;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.web.bind.annotation.RequestBody;

import com.enxoo.binding.FlowProcessing;
import com.enxoo.model.Lead;
import com.enxoo.model.StandardResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@Path("/apartments")
@Api(value = "apartmentsAPI")
@Consumes("application/json")
@Produces("application/json")
public class PostLead {

	private FlowProcessing flowProcessing;

	@POST
	@Path("/lead")
	@ApiOperation(value = "provides API for creating a new leads.", notes = "provides API for creating a single lead.", response = StandardResponse.class, code = 201)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Validation Error") })
	public Response createLead(@Valid @RequestBody Lead req) {

		String response = new String();

		int httpCode = 201;
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Lead>> errors = validator.validate(req);

		if (errors.size() > 0) {
			httpCode = 400;
			response = "{\"status\":\" " + httpCode + " \",\"message\":\"";
			for (ConstraintViolation<Lead> t : errors) {
				response += "[" + t.getPropertyPath().toString() + "]: [" + t.getMessage() + "] | ";
			}
			response += "\"}";
			return Response.status(httpCode).entity(response).build();
		}

		response = flowProcessing.postLead(req);

		try {
			int indexOf;
			indexOf = response.indexOf("statusCode");
			if (indexOf > 0) {
				httpCode = Integer.valueOf(response.substring(indexOf + 12, indexOf + 12 + 3));
				if (httpCode == 200) {
					httpCode = 201;
				}
			}
		} catch (Exception e) {
			httpCode = 500;
		}

		return Response.status(httpCode).entity(response).build();
	}

	public void setFlowProcessing(FlowProcessing flowProcessing) {
		this.flowProcessing = flowProcessing;
	}

}
