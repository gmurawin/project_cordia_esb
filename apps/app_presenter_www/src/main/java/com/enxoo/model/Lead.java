package com.enxoo.model;

import javax.validation.constraints.Size;



import com.enxoo.model.Lead;



public class Lead {
	@Size (max=40)
	private String firstName;
	@Size (max=20)
	private String lastName;
	private String phone;
	private String email;
	private String source;
	private String countryOfOrigin;
	private String investmentId;
	private String apartmentId;
	private String registeredBy;
	private Boolean commercialConsent;
	private Boolean marketingConsent;
	private Boolean commercialInformationConsent;
	private Boolean telephoneContactConsent;
	private Boolean gdprAgreement;
	private String subject;
	private String description;
	private Survey survey;
	private Boolean personalDataProcessingAgreement;
	private Boolean electronicCommunicationAgreement;
	private String firstContactType;
	
	
	public String getFirstContactType() {
		return firstContactType;
	}
	public void setFirstContactType(String firstContactType) {
		this.firstContactType = firstContactType;
	}
	public Boolean getPersonalDataProcessingAgreement() {
		return personalDataProcessingAgreement;
	}
	public void setPersonalDataProcessingAgreement(Boolean personalDataProcessingAgreement) {
		this.personalDataProcessingAgreement = personalDataProcessingAgreement;
	}
	public Boolean getElectronicCommunicationAgreement() {
		return electronicCommunicationAgreement;
	}
	public void setElectronicCommunicationAgreement(Boolean electronicCommunicationAgreement) {
		this.electronicCommunicationAgreement = electronicCommunicationAgreement;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}
	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}
	public String getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(String investmentId) {
		this.investmentId = investmentId;
	}
	public String getApartmentId() {
		return apartmentId;
	}
	public void setApartmentId(String apartmentId) {
		this.apartmentId = apartmentId;
	}
	public String getRegisteredBy() {
		return registeredBy;
	}
	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}
	public Survey getSurvey() {
		return survey;
	}
	public void setSurvey(Survey survey) {
		this.survey = survey;
	}
	public Boolean getCommercialConsent() {
		return commercialConsent;
	}
	public void setCommercialConsent(Boolean commercialConsent) {
		this.commercialConsent = commercialConsent;
	}
	public Boolean getMarketingConsent() {
		return marketingConsent;
	}
	public void setMarketingConsent(Boolean marketingConsent) {
		this.marketingConsent = marketingConsent;
	}
	public Boolean getCommercialInformationConsent() {
		return commercialInformationConsent;
	}
	public void setCommercialInformationConsent(Boolean commercialInformationConsent) {
		this.commercialInformationConsent = commercialInformationConsent;
	}
	public Boolean getTelephoneContactConsent() {
		return telephoneContactConsent;
	}
	public void setTelephoneContactConsent(Boolean telephoneContactConsent) {
		this.telephoneContactConsent = telephoneContactConsent;
	}
	public Boolean getGdprAgreement() {
		return gdprAgreement;
	}
	public void setGdprAgreement(Boolean gdprAgreement) {
		this.gdprAgreement = gdprAgreement;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
}
