package com.enxoo.model;

public class AdditionalElement {
	private String name;
	private Double area;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
	
}
