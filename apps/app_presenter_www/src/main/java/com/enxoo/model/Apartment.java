package com.enxoo.model;

import java.util.List;

public class Apartment {
	private String sfId;
    private String name;
    private String symbol;
    private String status;
    private String investmentId;
    private String type;
    private String agreementType;
    private Double  area;
    private Integer floorLevel;
    private Integer rooms;
    private String description;
    private Double  price;
    private Double  netPrice;
    private Double  pricePerSquareMeter;
    private Boolean showPrice;
    private Double promotionPrice;
    private String startOfPromo;
    private String endOfPromo;
    private String stageId;
    private String stageName;
    private String buildingNumber;
    private String buildingName;
    private String dateOfCompletion;
    private String staircase;
    private String kitchen;
    private String standardOfCompletion;
    private String worldSide;
    private Boolean isFinished;
    private String parkingSpaceLocation;
    private String parkingSpaceType;
    private String pdfUrl;
    private String imagesUrl;
    private Boolean showOnWeb;
    private String apartmentId;
    private List<AdditionalElement> additionalElements;
    
    
	public String getApartmentId() {
		return apartmentId;
	}
	public void setApartmentId(String apartmentId) {
		this.apartmentId = apartmentId;
	}
	public String getSfId() {
		return sfId;
	}
	public void setSfId(String sfId) {
		this.sfId = sfId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(String investmentId) {
		this.investmentId = investmentId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAgreementType() {
		return agreementType;
	}
	public void setAgreementType(String agreementType) {
		this.agreementType = agreementType;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
	public Integer getFloorLevel() {
		return floorLevel;
	}
	public void setFloorLevel(Integer floorLevel) {
		this.floorLevel = floorLevel;
	}
	public Integer getRooms() {
		return rooms;
	}
	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getNetPrice() {
		return netPrice;
	}
	public void setNetPrice(Double netPrice) {
		this.netPrice = netPrice;
	}
	public Double getPricePerSquareMeter() {
		return pricePerSquareMeter;
	}
	public void setPricePerSquareMeter(Double pricePerSquareMeter) {
		this.pricePerSquareMeter = pricePerSquareMeter;
	}
	public Boolean getShowPrice() {
		return showPrice;
	}
	public void setShowPrice(Boolean showPrice) {
		this.showPrice = showPrice;
	}
	public Double getPromotionPrice() {
		return promotionPrice;
	}
	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}
	public String getStartOfPromo() {
		return startOfPromo;
	}
	public void setStartOfPromo(String startOfPromo) {
		this.startOfPromo = startOfPromo;
	}
	public String getEndOfPromo() {
		return endOfPromo;
	}
	public void setEndOfPromo(String endOfPromo) {
		this.endOfPromo = endOfPromo;
	}
	public String getStageId() {
		return stageId;
	}
	public void setStageId(String stageId) {
		this.stageId = stageId;
	}
	public String getStageName() {
		return stageName;
	}
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}
	public String getBuildingNumber() {
		return buildingNumber;
	}
	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getDateOfCompletion() {
		return dateOfCompletion;
	}
	public void setDateOfCompletion(String dateOfCompletion) {
		this.dateOfCompletion = dateOfCompletion;
	}
	public String getStaircase() {
		return staircase;
	}
	public void setStaircase(String staircase) {
		this.staircase = staircase;
	}
	public String getKitchen() {
		return kitchen;
	}
	public void setKitchen(String kitchen) {
		this.kitchen = kitchen;
	}
	public String getStandardOfCompletion() {
		return standardOfCompletion;
	}
	public void setStandardOfCompletion(String standardOfCompletion) {
		this.standardOfCompletion = standardOfCompletion;
	}
	public String getWorldSide() {
		return worldSide;
	}
	public void setWorldSide(String worldSide) {
		this.worldSide = worldSide;
	}
	public Boolean getIsFinished() {
		return isFinished;
	}
	public void setIsFinished(Boolean isFinished) {
		this.isFinished = isFinished;
	}
	public String getParkingSpaceLocation() {
		return parkingSpaceLocation;
	}
	public void setParkingSpaceLocation(String parkingSpaceLocation) {
		this.parkingSpaceLocation = parkingSpaceLocation;
	}
	public String getParkingSpaceType() {
		return parkingSpaceType;
	}
	public void setParkingSpaceType(String parkingSpaceType) {
		this.parkingSpaceType = parkingSpaceType;
	}
	public String getPdfUrl() {
		return pdfUrl;
	}
	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}
	public String getImagesUrl() {
		return imagesUrl;
	}
	public void setImagesUrl(String imagesUrl) {
		this.imagesUrl = imagesUrl;
	}
	public Boolean getShowOnWeb() {
		return showOnWeb;
	}
	public void setShowOnWeb(Boolean showOnWeb) {
		this.showOnWeb = showOnWeb;
	}
	public List<AdditionalElement> getAdditionalElements() {
		return additionalElements;
	}
	public void setAdditionalElements(List<AdditionalElement> additionalElements) {
		this.additionalElements = additionalElements;
	}
	

	
	
	
}
