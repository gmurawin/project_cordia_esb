package com.enxoo.binding;

import java.util.List;

import com.enxoo.model.Apartment;
import com.enxoo.model.Lead;
import com.enxoo.model.ResourceDtoForPUSHService;

public interface FlowProcessing {
	String getInvestments(String req);

	String postLead(Lead req);

	String updateResourcePush(ResourceDtoForPUSHService req);

	String updateResourcePush(List<Apartment> req);
	
	String updateResourcePush(String req);

	String deleteResourcePush(ResourceDtoForPUSHService req);
}