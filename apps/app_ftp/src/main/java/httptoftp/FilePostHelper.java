package httptoftp;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang.RandomStringUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;

public class FilePostHelper implements Callable {

	final String FTP_ADDRESS = "ftp.address";
	final String PORT = "ftp.port";
	final String USER = "ftp.user";
	final String PASSWORD = "ftp.password";
	final String ADDRESS = "ftp.static_address";

	Properties prop;
	InputStream input = null;

	private String getFileName(String header, String filePath) {
		String fileExt = getFileNameFromHeader(header);
		fileExt = fileExt.substring(fileExt.lastIndexOf(".") + 1);

		String path = null;
		try {
			path = URLDecoder.decode(filePath, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return path + "/" + generateUniqueFileName() + "." + fileExt;
	}

	private String generateUniqueFileName() {
		String filename = "";
		long millis = System.currentTimeMillis();
		String datetime = new Date().toGMTString();
		datetime = datetime.replace(" ", "");
		datetime = datetime.replace(":", "");
		String rndchars = RandomStringUtils.randomAlphanumeric(16);
		filename = rndchars + "_" + datetime + "_" + millis;

		return filename;
	}

	private String getFileNameFromHeader(String header) {
		for (String content : header.split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim()
						.replace("\"", "");
			}
		}
		return null;
	}

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {

		String savePath = (String) eventContext.getMessage()
				.getInboundAttachment("savePath").getContent();

		String header = (String) eventContext.getMessage().getProperty(
				"contentVar", PropertyScope.SESSION);
		
		savePath = savePath.replaceAll("%20", "_");
		savePath = savePath.replaceAll(" ", "_");

		String filename = getFileName(header, savePath);
		// generated name of file
		eventContext.getMessage().setProperty("FileToSave", filename,
				PropertyScope.INVOCATION);
		System.out.println(filename);
		return eventContext.getMessage().getInboundAttachment("file")
				.getDataSource().getInputStream();
	}

}
