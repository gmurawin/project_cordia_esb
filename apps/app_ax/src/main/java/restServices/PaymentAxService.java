package restServices;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.force.api.ApiConfig;
import com.force.api.ResourceRepresentation;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import restCall.ForceApi;
import support.EmailSender;
import support.PropertiesHelper;

@Api(value = "/PaymentAx")
@Consumes("application/json")
@Produces("application/json")
@Path("/api/v1/payment")

public class PaymentAxService {
	private static Logger logger = LoggerFactory.getLogger(PaymentAxService.class);

	public static PropertiesHelper propertiesHelper = new PropertiesHelper();

	@POST
	@ApiOperation(value = "New payment from AX", authorizations = {@Authorization(value = "basicAuth")})
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "ok"), @ApiResponse(code = 422, message = "Validation exception"), 
			@ApiResponse(code = 500, message = "DML exception")})
	public static Response addPayment(@ApiParam(value = "Payment", required = true) String payment) {
		
		Integer responseCode = sendPaymentToSalesforce(payment);
		if (responseCode == 200) {
			new EmailSender("New Payment from AX", "Success: " + payment.toString()).sendInformationEmail();
			return Response.status(200).entity("ok").build();
		}
		if (responseCode == 512) {
			new EmailSender("New Payment from AX", "SF problem with deserialisation: " + payment.toString())
					.sendErrorEmail();
			return Response.status(512).entity("Problem with deserialisation").build();
		}
		return Response.status(responseCode).entity("ok").build();
	}

	public static Integer sendPaymentToSalesforce(String payment) {
		ApiConfig apiConfig = new ApiConfig();
		System.out.println("Test login: "+propertiesHelper.getSalesforceLogin()+ " password: "+propertiesHelper.getSalesforcePassword());
		apiConfig.setUsername(propertiesHelper.getSalesforceLogin());
		apiConfig.setPassword(propertiesHelper.getSalesforcePassword());
		apiConfig.setClientId(propertiesHelper.getSalesforceClientId());
		apiConfig.setClientSecret(propertiesHelper.getSalesforceSecret());
		apiConfig.setForceURL(propertiesHelper.getSalesforceHost());
		System.out.println("apiConfig: "+apiConfig.getLoginEndpoint());
		ForceApi api = new ForceApi(apiConfig);

		ResourceRepresentation res = api.post("apexrest/api/ax/payment/", payment);
		return res.getResponseCode();

	}
}
