package restServices;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import axPoster.AxHttpSender;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import model.Invoice;
import support.EmailSender;

@Api(value = "/Invoice")
@Consumes("application/json")
@Produces("application/json")
@Path("/invoice")
public class InvoiceService {
	private static final String axPath = "/api/WorkOrderSheet";	
	private static Logger logger = LoggerFactory.getLogger(InvoiceService.class);
	
	@POST
	@ApiOperation(value = "New Invoice from Salesforce", authorizations = {@Authorization(value = "basicAuth")})
	@Produces("application/json")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "ok")})
	public static Response addInvoice(@ApiParam(value = "Invoice", required = true) String invoice) {
		logger.debug("New Invoice from Salesforce: "+invoice.toString());
		
		logger.info("Created new invloce: "+invoice);
		
		AxHttpSender axHttpSender = new AxHttpSender(axPath, "NONE");
		axHttpSender.setHeader("content-type", "application/json; charset=utf-8");
		axHttpSender.addBody(invoice);
		HttpResponse httpResponse = axHttpSender.httpPost();
		logger.debug("Create new invoice: "+ invoice);
		
		if(httpResponse == null) {
			logger.debug("Connection Problem: "+"invoice: cannot send: "+invoice);
			new EmailSender("Invoice", "Connection problem: "+invoice).sendErrorEmail();
			return Response.status(500).entity("Connection problem").build();
		}
		logger.info("Invoice axsender response: "+httpResponse.getStatusLine().getStatusCode()+ " invoice json: "+invoice);
		new EmailSender("Invoice", "Success: "+invoice.toString()).sendInformationEmail();
		return Response.status(httpResponse.getStatusLine().getStatusCode()).build();
	}
}
