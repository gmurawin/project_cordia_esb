package restServices;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import axPoster.AxHttpSender;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import support.EmailSender;

@Api(value = "/customers")
@Produces("application/json")
@Consumes("application/json")
@Path("/customers")
public class CustomerService {
	private static Logger logger = LoggerFactory.getLogger(CustomerService.class);
	private static final String axPath = "/api/Customer";

	@POST
	@ApiOperation(httpMethod = "POST" , value = "New Customer from Salesforce", authorizations = {@Authorization(value = "basicAuth")})
	@ApiResponses(value = { @ApiResponse(code = 200, message = "New Customer from Salefsorce updated"),
			@ApiResponse(code = 500, message = "Connection problem with AX") })
	public Response addCustomer(@ApiParam(value = "new Customer", required = true) String customer) {
		AxHttpSender axHttpSender = new AxHttpSender(axPath, "NONE");
		axHttpSender.setHeader("content-type", "application/json; charset=utf-8");
		axHttpSender.addBody(customer);
		
		HttpResponse httpResponse = axHttpSender.httpPost();

		logger.debug("Create new customer: "+ customer);
		if (httpResponse == null) {
			logger.debug("Connection Problem: " + "customer cannot send: " + customer);
			new EmailSender("New customer", "Connection problem: "+customer).sendErrorEmail();
			return Response.status(500).entity("Connection problem").build();
		}
		new EmailSender("New customer", "Success json: "+customer).sendInformationEmail();
		return Response.status(httpResponse.getStatusLine().getStatusCode()).build();
	}

	@PUT
	@ApiOperation(httpMethod = "PUT", value = "New Customer from Salesforce", authorizations = {@Authorization(value = "basicAuth")})
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Customer from Salefsorce updated"),
			@ApiResponse(code = 500, message = "Connection problem with AX") })
	@Produces("application/json")
	public Response updateCustomer(@ApiParam(value = "new Customer", required = true) String customer) {

		logger.info("Test Created new customer: " + customer);
		logger.info("Customer as json: "+customer);

		AxHttpSender axHttpSender = new AxHttpSender(axPath, customer);
		axHttpSender.setHeader("content-type", "application/json;charset=UTF-8");

		axHttpSender.addBody(customer);
		HttpResponse httpResponse = axHttpSender.httpPut();
		if (httpResponse == null) {
			logger.debug("Connection Problem: " + "customer cannot send: " + customer);
			new EmailSender("New customer", "Connection problem: "+customer).sendErrorEmail();
			return Response.status(500).entity("Connection problem").build();
		}
		new EmailSender("New customer", "Success: "+customer).sendInformationEmail();
		return Response.status(httpResponse.getStatusLine().getStatusCode()).build();
	}

}
