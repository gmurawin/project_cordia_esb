package model;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

@ApiModel(value = "AgreementLine")
public class AgreementLine {

	@ApiModelProperty(value = "Salesforce customer id", example = "0010Y00000bNvoaQAC")
	public String customerAccountId;

	@ApiModelProperty(value = "Like header company id", example = "PD2")
	public String company;

	@ApiModelProperty(value = "Salesforce agreement id", example = "a0t0Y000002sLtCQAU")
	public String salesAgreementId;

	@ApiModelProperty(value = "Defines the type of service/product sold; "
			+ "structured by ledger account - maybe it can be calculated according to property, vat etc.", example = "flat sales")
	public String itemNo;

	@ApiModelProperty(value = "quantity", example = "1")
	public int quantity;

	@ApiModelProperty(value = "Has to be decided whether net amount will be imported or unit price and qty. Net amount = qty * unit price", example = "1300.00")
	public BigDecimal amount;

	@ApiModelProperty(value = "where we expect the money", example = "60109000049824025253848521")
	public String bankAccountCode;
	
	@ApiModelProperty(value = "currency ISO, can be different from header", example = "PLN")
	public String currencyIso;

	@ApiModelProperty(value = "Phase of the project when the payment (represented by the line) becomes due", allowableValues = "HANDOVER, PERMISSION, DOORWINDOW, FINISHING, FOUNDATION, FRONT_ROOF, BASEMNT_OK, STRUCTR_OK, LANDSCAPE", example = "HANDOVER")
	public String projectPhase;

	@ApiModelProperty(value = "Currently only \"normal\", \"penalty\" and \"reservation deposit\" are used", example = "normal")
	public String paymentCategory;

	@ApiModelProperty(value = "ID of the apartment/parking lot/storage room / etc being sold", example = "a0p1n0000036i1VAAQ")
	public String property;

	@ApiModelProperty(value = "Date when payment becomes due for the given payment schedule line", example = "2018-12-24")
	public String dueDate;

	@ApiModelProperty(value = "Date when payment becomes due at the latest if payment is tied to project phase. Can be left empty", example = "")
	public String notLaterThanDate;

	@ApiModelProperty(value = "can be calculated from property", example = "1_4_06")
	public String cFCode;

	@ApiModelProperty(value = "Percentage rate vat", example = "8")
	public Integer vatPct;

	@Override
	public String toString() {
		return "AgreementLine [customerAccountId=" + customerAccountId + ", company=" + company + ", salesAgreementId="
				+ salesAgreementId + ", itemNo=" + itemNo + ", quantity=" + quantity + ", amount=" + amount
				+ ", bankAccountCode=" + bankAccountCode + ", currencyIso=" + currencyIso + ", projectPhase="
				+ projectPhase + ", paymentCategory=" + paymentCategory + ", property=" + property + ", dueDate="
				+ dueDate + ", notLaterThanDate=" + notLaterThanDate + ", cFCode=" + cFCode + ", vatPct=" + vatPct
				+ "]";
	}


	

	
}
