package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Max;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import com.fasterxml.jackson.annotation.JsonProperty;

import agreement.CustomerShares;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Agreement Header")
public class AgreementHeader {

	@ApiModelProperty(value = "SF Account Id", allowableValues = "0010Y00000bNvoaQAC")
	public String customerAccount;

	public List<CustomerShares> customerAccounts;

	@ApiModelProperty(value = "AX value list", allowableValues = "PCD2, PCD3")
	public String company;

	@Max(4)
	@ApiModelProperty(value = "Currency ISO code", allowableValues = "CHF, CZK, EUR, GBP, HUF, ILS, JPY, PLN, RON, USD")
	public String salesAgreementId;

	@ApiModelProperty(value = "Defines the language of the customer", allowableValues = "hu, pl, en, en-za")
	public String languageId;

	@JsonProperty("currency")
	@ApiModelProperty(value = "Country code ISO-3166", allowableValues = "HUN, POL")
	public String currencyIso;

	public Date effectiveDate;

	public Date expirationDate;

	public Date reservationAgreementSignDate;

	public Date preliminarySignDate;

	public Date finalContractSignDate;

	public String documentTitle;

	@ApiModelProperty(value = "Current agreement type", allowableValues = "Reservation_cust, Final_cust, Preliminary_cust")
	public LegalType legalType;

	@ApiModelProperty(value = "if the agreement logically refers to an other agreement than it should be filled here", allowableValues = "0020Z00001bNvoaQBj")
	public String originalContractId;

	@ApiModelProperty(value = "Investment code", allowableValues = "PL_Bukow, PL_CORD, PL_CYS2")
	public String developmentProject;

	@ApiModelProperty(value = "Defines type of retention on the contract - most likely will not be used")
	public String retentionSetupCode;

	public String DefaultAgreementLineType;

	@ApiModelProperty(value = "Agreement status", allowableValues = "Finished, OnHold, Effective, Canceled")
	public Status status;

	public String salesResponsible;

	public String cRMResponsible;

	public enum Status {
		OnHold, Effective, Canceled, Finished
	}

	public enum LegalType {
		Reservation_cust, Final_cust, Preliminary_cust
	}

}
