package model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Max;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class Invoice {
	
	@Max(4)
	@ApiModelProperty(value = "AX value list", allowableValues = "PCD2, PCD3", example="PCD2")
	public String companyId;

	@ApiModelProperty(value = "SF Account Id", allowableValues = "0010Y00000bNvoaQAC", example = "0010Y00000bNvoaQAC")
	public String custAccount;

	@Max(20)
	@ApiModelProperty(value = "SF Invoice Id", allowableValues = "a0T0Y000000wY0MUAU", example="a0T0Y000000wY0MUAU")
	public String invoiceId;

	@Max(60)
	@ApiModelProperty(value = "Description", allowableValues = "Sample desc", example="Sample desc", required = false)
	public String description;

	@Max(4)
	@ApiModelProperty(value = "Currency ISO code", allowableValues = "CHF, CZK, EUR, GBP, HUF, ILS, JPY, PLN, RON, USD", example= "EUR")
	public String currencyIso;

	@ApiModelProperty(value = "Date from statement", example="2018-11-08")
	public String transDate;

	@ApiModelProperty(value = "Creation Date", example="2018-11-08")
	public String issueDate;

	@ApiModelProperty(value = "Same as Creation Date", example="2018-11-08")
	public String vatDate;
	
	@ApiModelProperty(value = "Due date", example = "2018-11-08")
	public String dueDate;

	@ApiModelProperty(value = "Invoice total (from all invoices). Gross", allowableValues = "1000.10, 100.2", example= "1000.20")
	public BigDecimal amount;

	@ApiModelProperty(value = "Proper reason code reference based on reason code table", 
			allowableValues = "CASH09, CASH10,CASH11,CASH12,CASH13,CASH14,REV01,REV02,REV03,REV04,REV05,REV06,REV07,REV08,REV09,REV10,REV11,REV12,REV13,REV14,REV15,REV16,REV17,REV18,REV19",
			example = "CASH09")
	public String reasonCode;
	
	@ApiModelProperty(value = "Agreement dimension - Salesforce agreement Id", example = "a0t0Y000002sLtCQAU")
    public String salesAgreementId;

	@ApiModelProperty(value = "Can be determined from property", example = "1_4_05")
    public String cfCode;

	@ApiModelProperty(value = "Development project Salesforce Id", example = "a0t0J000002sLtTFGS" )
    public String developmentProject;

	@ApiModelProperty(value = "Property Id", example = "a0t0J000004sLtYDUW")
    public String property;
	
	@ApiModelProperty(value = "Amount of vat in invoice", example = "8")
	public BigDecimal vatAmount;

	@ApiModelProperty(value = "Invoice Lines list")
	public List<InvoiceLine> lines;
	
	@ApiModelProperty(value = "Invoice type based on recordType", example = "Advance")
	public String invoiceType;

	@Override
	public String toString() {
		return "Invoice [companyId=" + companyId + ", custAccount=" + custAccount + ", invoiceId=" + invoiceId
				+ ", description=" + description + ", currencyIso=" + currencyIso + ", transDate=" + transDate
				+ ", issueDate=" + issueDate + ", vatDate=" + vatDate + ", dueDate=" + dueDate + ", amount=" + amount
				+ ", reasonCode=" + reasonCode + ", salesAgreementId=" + salesAgreementId + ", cfCode=" + cfCode
				+ ", developmentProject=" + developmentProject + ", property=" + property + ", vatAmount=" + vatAmount
				+ ", lines=" + lines + ", invoiceType=" + invoiceType + "]";
	}
	


    
	
}
