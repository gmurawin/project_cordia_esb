package model;

import io.swagger.annotations.ApiModelProperty;

public class Property {
	@ApiModelProperty(value = "The full name", allowableValues = "value", example = "value")
	public String name;

	@ApiModelProperty(value = "FlatApartment or CommercialProperty", allowableValues = "value", example = "FlatApartment")
	public String type;

	@ApiModelProperty(value = "The unique Id", allowableValues = "value", example = "0010Y00000bNvoaQAC")
	public String id;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "Reservation")
	public String status;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "Finished")
	public String constructionStatus;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "0010Y00000bNvoaQAZ")
	public String buildingId;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "1")
	public String number;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "1")
	public String finalNumber;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "value")
	public String bankAccountNumber;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "10.1")
	public String buildArea;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "9.1")
	public Double usableArea;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "2.0")
	public Double rooms;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "1.0")
	public Double floorLevel;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "1.0")
	public Double bathrooms;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "PANORAMA")
	public String worldSide;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "10.0")
	public Double price;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "7.1")
	public Double netPrice;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "9.0")
	public Double minimumPrice;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "9.0")
	public Double budgetPrice;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "PLN")
	public String currencyIsoCode;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "1.0")
	public Double areaMeasured;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "0.5")
	Double finalPriceAfterMeasurements;

	@ApiModelProperty(value = "test", allowableValues = "value", example = "0.6")
	public Double finalNetPriceAfterMeasurements;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "2.0")
	public Double balconyArea;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "1.0")
	public Double loggiaArea;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "3.0")
	public Double terraceArea;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "5.1")
	public Double gardenArea;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "0.0")
	public Double winterGardenArea;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "true")
	public Boolean separateWc;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "true")
	public Boolean wellPlannedFlat;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "true")
	public Boolean separateKitchen;
	
	@ApiModelProperty(value = "test", allowableValues = "value", example = "true")
	public Boolean possibleSeparateKitchen;
	
	@Override
	public String toString() {
		return "Property [name=" + name + ", type=" + type + ", id=" + id + ", status=" + status
				+ ", constructionStatus=" + constructionStatus + ", buildingId=" + buildingId + ", number=" + number
				+ ", finalNumber=" + finalNumber + ", bankAccountNumber=" + bankAccountNumber + ", buildArea="
				+ buildArea + ", usableArea=" + usableArea + ", rooms=" + rooms + ", floorLevel=" + floorLevel
				+ ", bathrooms=" + bathrooms + ", worldSide=" + worldSide + ", price=" + price + ", netPrice="
				+ netPrice + ", minimumPrice=" + minimumPrice + ", budgetPrice=" + budgetPrice + ", currencyIsoCode="
				+ currencyIsoCode + ", areaMeasured=" + areaMeasured + ", finalPriceAfterMeasurements="
				+ finalPriceAfterMeasurements + ", finalNetPriceAfterMeasurements=" + finalNetPriceAfterMeasurements
				+ ", balconyArea=" + balconyArea + ", loggiaArea=" + loggiaArea + ", terraceArea=" + terraceArea
				+ ", gardenArea=" + gardenArea + ", winterGardenArea=" + winterGardenArea + ", separateWc=" + separateWc
				+ ", wellPlannedFlat=" + wellPlannedFlat + ", separateKitchen=" + separateKitchen
				+ ", possibleSeparateKitchen=" + possibleSeparateKitchen + "]";
	}
	
	
	
}
