package model;

import java.math.BigDecimal;
import java.sql.Date;

import javax.validation.constraints.Max;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



@ApiModel(value = "Payment")
public class Payment {
	

	@Max(4)
	@ApiModelProperty(value = "AX value list", allowableValues = "PCD2, PCD3", required = true, example="PCD2")
	public String Company;
	
	@Max(30)
	@ApiModelProperty(value = "normal account number receiver", required = true,
	example = "2345723495837485934834535")
	public String BankAccount;
	
	@Max(10)
	@ApiModelProperty(value = "SF statement Id - prevent double import", required = true, example = "a0T0Y000000wY0MUAU")
	public String StatementNo;
	
	@ApiModelProperty(value = "Date from statement YYYY-MM-DD", required = true, example = "2018-10-11")
	public Date TransDate;
	
	@ApiModelProperty(value = "Description", required = true, example = "Sample description")
	public String Description;
	
	@Max(4)
	@ApiModelProperty(value = "Currency ISO code", allowableValues = "CHF, CZK, EUR, GBP, HUF, ILS, JPY, PLN, RON, USD", required = true, example = "EUR")
	public String CurrencyCode;
	
	@ApiModelProperty(value = "Gross amount", example = "11.1", required = true)
	public BigDecimal Amount;
	
	@Max(30)
	@ApiModelProperty(value = "normal account number sender", example = "97886307268296343653770115", required = true)
	public String SenderBankAccount;
	
	@ApiModelProperty(value = "SF agreement id", example = "a0t0Y0000014W7OQAU", required = true)
	public String SalesAgreementId;
	
	
	@ApiModelProperty(value = "Customer Salesforce id", example = "0010Q00000A2moWQAR", required = false)
	public String CustomerId;
	
	@ApiModelProperty(value = "Payment type", example ="Payment", required = true)
	public String PaymentType;
	
	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		String jsonObject = null;
		try {
			jsonObject = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

	




}
