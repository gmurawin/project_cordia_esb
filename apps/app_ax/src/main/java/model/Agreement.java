package model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Max;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import agreement.CustomerShares;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "Agreement")
public class Agreement {
	
	@ApiModelProperty(value = "SF Account Id", allowableValues = "0010Y00000bNvoaQAC", example = "0010Y00000bNvoaQAC")
	public String custAccount;

	public List<CustomerShares> customers;
	
	public List<AgreementLine> lines;

	@ApiModelProperty(value = "AX value list", allowableValues = "PCD2, PCD3", example = "PD2")
	public String companyId;

	@Max(4)
	@ApiModelProperty(value = "Salesforce agreement id", example = "a0t0Y000002sLtCQAU")
	public String salesAgreementId;

	@ApiModelProperty(value = "Defines the language of the customer", allowableValues = "hu, pl, en, en-za", example = "pl")
	public String languageId;

	@ApiModelProperty(value = "Currency ISO code", allowableValues = "CHF, CZK, EUR, GBP, HUF, ILS, JPY, PLN, RON, USD", example = "PLN")
	public String currencyIso;

	@ApiModelProperty(value = "Start date of contract", allowableValues = "2018-11-08", example = "2018-11-08")
	public String effectiveDate;

	@ApiModelProperty(value = "Can be left empty")
	public String expirationDate;

	@ApiModelProperty(value= "Reservation Agreement signed Date", example="2018-10-28")
	public String reservationAgreementSignDate;

	@ApiModelProperty(value = "Developer agreement signed", example = "2018-10-29")
	public String preliminarySignDate;

	@ApiModelProperty(value = "Final Agreement signed date", allowableValues = "2018-11-01", example = "2018-11-01")
	public String finalContractSignDate;

	@ApiModelProperty(value = "document title, can be left empty")
	public String documentTitle;

	@ApiModelProperty(value = "Current agreement type", allowableValues = "Reservation_cust, Final_cust, Preliminary_cust", example="Final_cust")
	public LegalType legalType;

	@ApiModelProperty(value = "if the agreement logically refers to an other agreement than it should be filled here", allowableValues = "0020Z00001bNvoaQBj", example = "0020Z00001bNvoaQBj")
	public String originalContractId;

	@ApiModelProperty(value = "Investment code", allowableValues = "PL_Bukow, PL_CORD, PL_CYS2", example = "PL_CORD")
	public String developmentProject;

	@ApiModelProperty(value = "Defines type of retention on the contract - most likely will not be used")
	public String retentionSetupCode;

	@ApiModelProperty(value = "Product value comm., product qty comm., etc can be empty")
	public String defaultAgreementLineType;

	@ApiModelProperty(value = "Agreement status", allowableValues = "Finished, OnHold, Effective, Canceled", example = "Effective")
	public Status status;

	@ApiModelProperty(value = "can be left empty")
	public String salesResponsible;

	@ApiModelProperty(value = "can be left empty")
	public String cRMResponsible;

	public enum Status {
		OnHold, Effective, Canceled, Finished
	}

	public enum LegalType {
		Reservation_cust, Final_cust, Preliminary_cust
	}

	@Override
	public String toString() {
		return "Agreement [custAccount=" + custAccount + ", customers=" + customers + ", lines=" + lines
				+ ", companyId=" + companyId + ", salesAgreementId=" + salesAgreementId + ", languageId=" + languageId
				+ ", currencyIso=" + currencyIso + ", effectiveDate=" + effectiveDate + ", expirationDate="
				+ expirationDate + ", reservationAgreementSignDate=" + reservationAgreementSignDate
				+ ", preliminarySignDate=" + preliminarySignDate + ", finalContractSignDate=" + finalContractSignDate
				+ ", documentTitle=" + documentTitle + ", legalType=" + legalType + ", originalContractId="
				+ originalContractId + ", developmentProject=" + developmentProject + ", retentionSetupCode="
				+ retentionSetupCode + ", defaultAgreementLineType=" + defaultAgreementLineType + ", status=" + status
				+ ", salesResponsible=" + salesResponsible + ", cRMResponsible=" + cRMResponsible + "]";
	}





	
}
