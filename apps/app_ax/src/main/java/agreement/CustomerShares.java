package agreement;

import io.swagger.annotations.ApiModelProperty;

public class CustomerShares {
	@ApiModelProperty(value = "Client shares", allowableValues = "50, 45, 100", example = "100")
	public Float share;

	@ApiModelProperty(value = "SF Account Id", allowableValues = "0010Y00000bNvoaQAC")
	public String custAccount;

	public CustomerShares() {

	}

	public Float getShares() {
		return share;
	}

	public void setShares(Float share) {
		this.share = share;
	}

	public String getCustAccount() {
		return custAccount;
	}

	public void setCustAccount(String custAccount) {
		this.custAccount = custAccount;
	}

	@Override
	public String toString() {
		return "CustomerShares [share=" + share + ", custAccount=" + custAccount + "]";
	}

}

