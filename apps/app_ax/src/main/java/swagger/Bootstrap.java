package swagger;

import com.sun.jersey.spi.resource.Singleton;
import io.swagger.models.Info;
import io.swagger.models.License;
import io.swagger.models.Swagger;
import io.swagger.models.auth.BasicAuthDefinition;
import io.swagger.mule.ApiListingJSON;

@Singleton
public class Bootstrap {
  /** Run on app init by Spring */
  public void start() {
    Info info = new Info()
      .title("Swagger Sample Cordia")
      .description("This is a sample demo API server")
      .license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html"))
      .version("1.0.0");

    Swagger swagger = new Swagger().info(info).basePath("/").produces("application/json");
    swagger.addSecurityDefinition("basicAuth", new BasicAuthDefinition());
    ApiListingJSON.init(swagger);
    
    
  }

}