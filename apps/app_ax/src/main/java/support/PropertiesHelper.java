package support;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesHelper {
	private static Logger logger = LoggerFactory.getLogger(PropertiesHelper.class);

	private String salesforceLogin;

	private String salesforcePassword;

	private String salesforceClientId;

	private String salesforceSecret;

	private String salesforceHost;
	
	private String ftpLogin;
	
	private String ftpPassword;
	
	private String ftpAddress;
	
	private String ftpPort;
	
	private String smtpHost;
	
	private String smtpSocketFactoryPort;
	
	private String smtpPort;
	
	private String smtpAddress;
	
	private String smtpPassword;
	
	private String smtpOutEmailAddress;
	
	private Boolean emailsEnable;
	
	private String axHost;
	private String axPort;
	private String axProtocol;
	

	public Boolean getEmailsEnable() {
		return emailsEnable;
	}

	public String getOutEmailAddress() {
		return smtpOutEmailAddress;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public String getSmtpSocketFactoryPort() {
		return smtpSocketFactoryPort;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public String getSmtpAddress() {
		return smtpAddress;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		PropertiesHelper.logger = logger;
	}

	public String getSalesforceLogin() {
		return salesforceLogin;
	}

	public void setSalesforceLogin(String salesforceLogin) {
		this.salesforceLogin = salesforceLogin;
	}

	public String getSalesforcePassword() {
		return salesforcePassword;
	}

	public void setSalesforcePassword(String salesforcePassword) {
		this.salesforcePassword = salesforcePassword;
	}

	public String getSalesforceClientId() {
		return salesforceClientId;
	}

	public void setSalesforceClientId(String salesforceClientId) {
		this.salesforceClientId = salesforceClientId;
	}

	public String getSalesforceSecret() {
		return salesforceSecret;
	}

	public void setSalesforceSecret(String salesforceSecret) {
		this.salesforceSecret = salesforceSecret;
	}

	public String getSalesforceHost() {
		return salesforceHost;
	}

	public void setSalesforceHost(String salesforceHost) {
		this.salesforceHost = salesforceHost;
	}

	public String getFtpLogin() {
		return ftpLogin;
	}

	public void setFtpLogin(String ftpLogin) {
		this.ftpLogin = ftpLogin;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	
	public String getFtpAddress() {
		return ftpAddress;
	}

	public void setFtpAddress(String ftpAddress) {
		this.ftpAddress = ftpAddress;
	}

	public String getFtpPort() {
		return ftpPort;
	}

	public void setFtpPort(String ftpPort) {
		this.ftpPort = ftpPort;
	}
	
	public String getAxHost() {
		return axHost;
	}

	public void setAxHost(String axHost) {
		this.axHost = axHost;
	}

	public String getAxPort() {
		return axPort;
	}

	public void setAxPort(String axPort) {
		this.axPort = axPort;
	}

	public String getAxProtocol() {
		return axProtocol;
	}

	public void setAxProtocol(String axProtocol) {
		this.axProtocol = axProtocol;
	}

	public PropertiesHelper() {
		String filename = "mule-domain.properties";
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = PropertiesHelper.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				logger.info("Sorry, unable to find " + filename);
				return;
			}
			prop.load(input);
			this.setSalesforceLogin(prop.getProperty("salesforce.user"));
			this.setSalesforcePassword(prop.getProperty("salesforce.password"));
			this.setSalesforceClientId(prop.getProperty("salesforce.clientId"));
			this.setSalesforceHost(prop.getProperty("salesforce.salesforceHost"));
			this.setSalesforceSecret(prop.getProperty("salesforce.clientSecret"));
			this.setFtpLogin(prop.getProperty("ftp.user"));
			this.setFtpPassword(prop.getProperty("ftp.password"));
			this.setFtpAddress(prop.getProperty("ftp.address"));
			this.setFtpPort(prop.getProperty("ftp.port"));
			this.smtpHost = prop.getProperty("mail.smtp.host");
			this.smtpSocketFactoryPort = prop.getProperty("mail.smtp.socketFactory.port");
			this.smtpPort = prop.getProperty("mail.smtp.port");
			this.smtpAddress = prop.getProperty("mail.smtp.address");
			this.smtpPassword = prop.getProperty("mail.smtp.password");
			this.smtpOutEmailAddress = prop.getProperty("mail.smtp.out.address");
			this.emailsEnable = Boolean.valueOf((String) prop.get("mail.information.enable"));
			
			// AX config
			this.axHost = prop.getProperty("service.ax.host");
			this.axProtocol = prop.getProperty("service.ax.protocol");
			this.axPort = prop.getProperty("service.ax.port");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
