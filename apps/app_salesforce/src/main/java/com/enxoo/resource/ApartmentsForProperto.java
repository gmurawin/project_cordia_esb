package com.enxoo.resource;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.web.bind.annotation.RequestBody;

import com.enxoo.binding.FlowProcessing;
import com.enxoo.model.ApartmentsApartmentsRequest;
import com.enxoo.model.ApartmentsSingleApartmentRequest;
import com.enxoo.model.StandardResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;


@Path("/apartments")
@Api(value="apartmentsAPI")
@Consumes("application/json")
@Produces("application/json")
public class ApartmentsForProperto {
	
	private FlowProcessing flowProcessing;
	
	@PUT	
	@Path("/apartment")
	@ApiOperation(value="provides API for updating a single apartment."
	,notes = "provides API for creating a single apartment."
	,response = StandardResponse.class
	,code = 201)
	@ApiResponses(value = {
			@ApiResponse(code=400, message = "Validation Error")})
	public Response updateApartment (@Valid @RequestBody ApartmentsSingleApartmentRequest req){
				
		
		String response = new String();		
		int httpCode = 201;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ApartmentsSingleApartmentRequest>>errors=validator.validate(req);
            
        if (errors.size()>0){
        	httpCode = 400;
        	response = "{\"status\":\" "+ httpCode +" \",\"message\":\"";
        	for (ConstraintViolation<ApartmentsSingleApartmentRequest> t : errors){
        		response += "[" + t.getPropertyPath().toString() + "]: ["+ t.getMessage()+ "] | ";
        	}
        	response += "\"}";
        	return Response.status(httpCode).entity(response).build();
        }
       
		response = flowProcessing.updateSingleApartment(req);
		
		return Response.status(httpCode).entity(response).build();
	}
	
	@POST	
	@Path("/apartment")
	@ApiOperation(value="provides API for creating a new apartments."
	,notes = "provides API for creating a single apartment."
	,response = StandardResponse.class
	,code = 201)
	@ApiResponses(value = {
			@ApiResponse(code=400, message = "Validation Error")})
	public Response massCreateApartments (@Valid @RequestBody ApartmentsApartmentsRequest req){
				
		
		String response = new String();		
		int httpCode = 201;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ApartmentsApartmentsRequest>>errors=validator.validate(req);
            
        if (errors.size()>0){
        	httpCode = 400;
        	response = "{\"status\":\" "+ httpCode +" \",\"message\":\"";
        	for (ConstraintViolation<ApartmentsApartmentsRequest> t : errors){
        		response += "[" + t.getPropertyPath().toString() + "]: ["+ t.getMessage()+ "] | ";
        	}
        	response += "\"}";
        	return Response.status(httpCode).entity(response).build();
        }
       
		response = flowProcessing.massCreateApartments(req);
		
		return Response.status(httpCode).entity(response).build();
	}
	
	@DELETE	
	@Path("/apartment/{apartmentId}")
	@ApiOperation(value="provides API for deleting a single apartment."
	,notes = "provides API for deleting a single apartment."
	,response = StandardResponse.class
	,code = 200)
	@ApiResponses(value = {
			@ApiResponse(code=400, message = "Validation Error")})
	public Response deleteApartment (@Context UriInfo UriInfo, @PathParam("apartmentId") String apartmentId){
				
		String response = new String();		
		int httpCode = 200;
       
		response = flowProcessing.deleteApartment(apartmentId);
		
		return Response.status(httpCode).entity(response).build();
	}	

	
	public void setFlowProcessing(FlowProcessing flowProcessing) {
		this.flowProcessing = flowProcessing;
	}	
	
}
