package com.enxoo.resource;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.web.bind.annotation.RequestBody;

import com.enxoo.binding.FlowProcessing;
import com.enxoo.model.CustomerGctPatchRequest;
import com.enxoo.model.CustomerGctRequest;
import com.enxoo.model.CustomerGctResponse;
import com.enxoo.model.CustomerGctSimpleResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;


@Path("/gct")
@Api(value="gctAPI")
@Consumes("application/json")
@Produces("application/json")
public class GctCustomer {
	
	private FlowProcessing flowProcessing;
	

	@POST	
	@Path("/customer")
	@ApiOperation(value="provides API for creating customer in GCT"
	,notes = "provides API for creating customer in GCT."
	,response = CustomerGctResponse.class
	,code = 201)
	@ApiResponses(value = {
			@ApiResponse(code=400, message = "Validation Error")})
	public Response createCustomerInGct (@Valid @RequestBody CustomerGctRequest req){
				
		
		String response = new String();		
		int httpCode = 201;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<CustomerGctRequest>>errors=validator.validate(req);
            
        if (errors.size()>0){
        	httpCode = 400;
        	response = "{\"status\":\" "+ httpCode +" \",\"message\":\"";
        	for (ConstraintViolation<CustomerGctRequest> t : errors){
        		response += "[" + t.getPropertyPath().toString() + "]: ["+ t.getMessage()+ "] | ";
        	}
        	response += "\"}";
        	return Response.status(httpCode).entity(response).build();
        }
       
		response = flowProcessing.createCustomerInGct(req);
		
		return Response.status(httpCode).entity(response).build();
	}
	
	@PATCH	
	@Path("/customer")
	@ApiOperation(value="provides API for updating customer in GCT"
	,notes = "provides API for creating updating in GCT."
	,response = CustomerGctSimpleResponse.class
	,code = 201)
	@ApiResponses(value = {
			@ApiResponse(code=400, message = "Validation Error")})
	public Response patchCustomerInGct (@Valid @RequestBody CustomerGctPatchRequest req){
				
		
		String response = new String();		
		int httpCode = 201;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<CustomerGctPatchRequest>>errors=validator.validate(req);
            
        if (errors.size()>0){
        	httpCode = 400;
        	response = "{\"status\":\" "+ httpCode +" \",\"message\":\"";
        	for (ConstraintViolation<CustomerGctPatchRequest> t : errors){
        		response += "[" + t.getPropertyPath().toString() + "]: ["+ t.getMessage()+ "] | ";
        	}
        	response += "\"}";
        	return Response.status(httpCode).entity(response).build();
        }
       
		response = flowProcessing.patchCustomerInGct(req);
		
		return Response.status(httpCode).entity(response).build();
	}
	

	
	public void setFlowProcessing(FlowProcessing flowProcessing) {
		this.flowProcessing = flowProcessing;
	}	
	
}
