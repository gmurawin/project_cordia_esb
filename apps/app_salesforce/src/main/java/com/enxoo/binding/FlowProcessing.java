package com.enxoo.binding;

import com.enxoo.model.AccountingEventFromProperto;
import com.enxoo.model.ApartmentsApartmentsRequest;
import com.enxoo.model.ApartmentsSingleApartmentRequest;
import com.enxoo.model.CustomerGctPatchRequest;
import com.enxoo.model.CustomerGctRequest;

public interface FlowProcessing {
	String createAccountingEvent (AccountingEventFromProperto req);
	String createCustomerInGct (CustomerGctRequest req);
	String patchCustomerInGct (CustomerGctPatchRequest req);
	String createSingleApartment (ApartmentsSingleApartmentRequest req);
	String updateSingleApartment (ApartmentsSingleApartmentRequest req);
	String massCreateApartments (ApartmentsApartmentsRequest req);
	String deleteApartment (String req);
}