package com.enxoo.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CustomerGctRequest {

	@Size (max=40)
	private String firstName;
	@Size (max=20)
	private String secondName;
	@Size (max=64)
	private String companyName;
	@NotNull
	@Size (min=15, max=30)
	private String propertoId; 
	@NotNull
	private String bankAccountNumber;
	@NotNull
	private String bankEscrowAccountNumber;
	@NotNull
	@Size (max = 25)
	private String investmentId;
	@NotNull
	@Size (max = 255)
	private String appartmentName;
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPropertoId() {
		return propertoId;
	}
	public void setPropertoId(String propertoId) {
		this.propertoId = propertoId;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getBankEscrowAccountNumber() {
		return bankEscrowAccountNumber;
	}
	public void setBankEscrowAccountNumber(String bankEscrowAccountNumber) {
		this.bankEscrowAccountNumber = bankEscrowAccountNumber;
	}
	public String getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(String investmentId) {
		this.investmentId = investmentId;
	}
	public String getAppartmentName() {
		return appartmentName;
	}
	public void setAppartmentName(String appartmentName) {
		this.appartmentName = appartmentName;
	}
	
	
	
}
