package com.enxoo.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApartmentsApartmentsResponse{

	@NotNull
	@Size (min = 1)
	private List<ApartmentsInvestment> investments;

	public List<ApartmentsInvestment> getInvestments() {
		return investments;
	}

	public void setInvestments(List<ApartmentsInvestment> investments) {
		this.investments = investments;
	}	
	
	
	
}
