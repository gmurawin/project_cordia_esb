package com.enxoo.model;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class AccountingEventFromProperto {

	@NotNull
	@Size (min = 18, message="Lenght of this element must be 18 char. minimum")
	private String customerId;
	@NotNull
	@Size (min = 18, message="Lenght of this element must be 18 char. minimum")
	private String rowId;
	@NotNull
	private String userName;
	private String appName;
	private String operName;
	private Date createdDate;
	private Date lastModifiedDate;
	private Date statementDate;
	private Double formerPaymentSum;
	private Double formerTransferSum;
	private String gtcAnaliticAcc;
	private String gtcModReglament;
	private String investmentAccuntingMainAccNumber;
	private String companyAccoutingCode;
	private String docNumber;
	private String investmentCode;
	private Double amount;
	private Double netAmount;
	private Double priceTotal;
	private String howToSettleDeposit;
	private Double depositAmount;
	private String howToSettleMainAccountPayment;
	private String howToHandlePurchaseResgination;
	private String howToHandleInterests;
	private String howToHandleagreementAnexCash;
	private String hoToHandleIncorrectPayment;
	private String howToHandleResignationFromAdditionalProduct;
	private Date agreeSignedDate;
	private String anexCause;
	
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getRowId() {
		return rowId;
	}
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getOperName() {
		return operName;
	}
	
	public void setOperName(String operName) {
		this.operName = operName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public Date getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(Date statementDate) {
		this.statementDate = statementDate;
	}
	public Double getFormerPaymentSum() {
		return formerPaymentSum;
	}
	public void setFormerPaymentSum(Double formerPaymentSum) {
		this.formerPaymentSum = formerPaymentSum;
	}
	public Double getFormerTransferSum() {
		return formerTransferSum;
	}
	public void setFormerTransferSum(Double formerTransferSum) {
		this.formerTransferSum = formerTransferSum;
	}
	public String getGtcAnaliticAcc() {
		return gtcAnaliticAcc;
	}
	public void setGtcAnaliticAcc(String gtcAnaliticAcc) {
		this.gtcAnaliticAcc = gtcAnaliticAcc;
	}
	public String getGtcModReglament() {
		return gtcModReglament;
	}
	public void setGtcModReglament(String gtcModReglament) {
		this.gtcModReglament = gtcModReglament;
	}
	public String getInvestmentAccuntingMainAccNumber() {
		return investmentAccuntingMainAccNumber;
	}
	public void setInvestmentAccuntingMainAccNumber(String investmentAccuntingMainAccNumber) {
		this.investmentAccuntingMainAccNumber = investmentAccuntingMainAccNumber;
	}
	public String getCompanyAccoutingCode() {
		return companyAccoutingCode;
	}
	public void setCompanyAccoutingCode(String companyAccoutingCode) {
		this.companyAccoutingCode = companyAccoutingCode;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getInvestmentCode() {
		return investmentCode;
	}
	public void setInvestmentCode(String investmentCode) {
		this.investmentCode = investmentCode;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}
	public Double getPriceTotal() {
		return priceTotal;
	}
	public void setPriceTotal(Double priceTotal) {
		this.priceTotal = priceTotal;
	}
	public String getHowToSettleDeposit() {
		return howToSettleDeposit;
	}
	public void setHowToSettleDeposit(String howToSettleDeposit) {
		this.howToSettleDeposit = howToSettleDeposit;
	}
	public Double getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(Double depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String getHowToSettleMainAccountPayment() {
		return howToSettleMainAccountPayment;
	}
	public void setHowToSettleMainAccountPayment(String howToSettleMainAccountPayment) {
		this.howToSettleMainAccountPayment = howToSettleMainAccountPayment;
	}
	public String getHowToHandlePurchaseResgination() {
		return howToHandlePurchaseResgination;
	}
	public void setHowToHandlePurchaseResgination(String howToHandlePurchaseResgination) {
		this.howToHandlePurchaseResgination = howToHandlePurchaseResgination;
	}
	public String getHowToHandleInterests() {
		return howToHandleInterests;
	}
	public void setHowToHandleInterests(String howToHandleInterests) {
		this.howToHandleInterests = howToHandleInterests;
	}
	public String getHowToHandleagreementAnexCash() {
		return howToHandleagreementAnexCash;
	}
	public void setHowToHandleagreementAnexCash(String howToHandleagreementAnexCash) {
		this.howToHandleagreementAnexCash = howToHandleagreementAnexCash;
	}

	public String getHoToHandleIncorrectPayment() {
		return hoToHandleIncorrectPayment;
	}
	public void setHoToHandleIncorrectPayment(String hoToHandleIncorrectPayment) {
		this.hoToHandleIncorrectPayment = hoToHandleIncorrectPayment;
	}
	public String getHowToHandleResignationFromAdditionalProduct() {
		return howToHandleResignationFromAdditionalProduct;
	}
	public void setHowToHandleResignationFromAdditionalProduct(String howToHandleResignationFromAdditionalProduct) {
		this.howToHandleResignationFromAdditionalProduct = howToHandleResignationFromAdditionalProduct;
	}
	public Date getAgreeSignedDate() {
		return agreeSignedDate;
	}
	public void setAgreeSignedDate(Date agreeSignedDate) {
		this.agreeSignedDate = agreeSignedDate;
	}
	public String getAnexCause() {
		return anexCause;
	}
	public void setAnexCause(String anexCause) {
		this.anexCause = anexCause;
	}
	
}
