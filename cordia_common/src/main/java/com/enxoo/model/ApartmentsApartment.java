package com.enxoo.model;

import java.sql.Date;
import java.util.List;

public class ApartmentsApartment{
	
	private String sfId;
	private String type;
	private String agreementType;
	private String symbol;
	private String status;
	private Double area;
	private Double price;
	private Double pricePerSquareMeter;
	private Boolean showPrice;
	private Double promotionPrice;
	private Date startOfPromo;
	private Date endOfPromo;
	private Integer floorLevel;
	private Integer noOfRooms;
	private String staircase;
	private String address;
	private String kitchen;
	private String standardOfCompletion;
	private String worldSide;
	private Double balconyArea;
	private Double loggiaArea;
	private Double terraceArea;
	private Double gardenArea;
	private Double porchArea;
	private Double orangeryArea;
	private Boolean isFinished;
	private String pdfUrl;
	private String imageUrl;
	private List<ApartmentsApartmentAdditionalElements> additionalElements;
	public String getSfId() {
		return sfId;
	}
	public void setSfId(String sfId) {
		this.sfId = sfId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAgreementType() {
		return agreementType;
	}
	public void setAgreementType(String agreementType) {
		this.agreementType = agreementType;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getPricePerSquareMeter() {
		return pricePerSquareMeter;
	}
	public void setPricePerSquareMeter(Double pricePerSquareMeter) {
		this.pricePerSquareMeter = pricePerSquareMeter;
	}
	public Boolean getShowPrice() {
		return showPrice;
	}
	public void setShowPrice(Boolean showPrice) {
		this.showPrice = showPrice;
	}
	public Double getPromotionPrice() {
		return promotionPrice;
	}
	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}
	public Date getStartOfPromo() {
		return startOfPromo;
	}
	public void setStartOfPromo(Date startOfPromo) {
		this.startOfPromo = startOfPromo;
	}
	public Date getEndOfPromo() {
		return endOfPromo;
	}
	public void setEndOfPromo(Date endOfPromo) {
		this.endOfPromo = endOfPromo;
	}
	public Integer getFloorLevel() {
		return floorLevel;
	}
	public void setFloorLevel(Integer floorLevel) {
		this.floorLevel = floorLevel;
	}
	public Integer getNoOfRooms() {
		return noOfRooms;
	}
	public void setNoOfRooms(Integer noOfRooms) {
		this.noOfRooms = noOfRooms;
	}
	public String getStaircase() {
		return staircase;
	}
	public void setStaircase(String staircase) {
		this.staircase = staircase;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getKitchen() {
		return kitchen;
	}
	public void setKitchen(String kitchen) {
		this.kitchen = kitchen;
	}
	public String getStandardOfCompletion() {
		return standardOfCompletion;
	}
	public void setStandardOfCompletion(String standardOfCompletion) {
		this.standardOfCompletion = standardOfCompletion;
	}
	public String getWorldSide() {
		return worldSide;
	}
	public void setWorldSide(String worldSide) {
		this.worldSide = worldSide;
	}
	public Double getBalconyArea() {
		return balconyArea;
	}
	public void setBalconyArea(Double balconyArea) {
		this.balconyArea = balconyArea;
	}
	public Double getLoggiaArea() {
		return loggiaArea;
	}
	public void setLoggiaArea(Double loggiaArea) {
		this.loggiaArea = loggiaArea;
	}
	public Double getTerraceArea() {
		return terraceArea;
	}
	public void setTerraceArea(Double terraceArea) {
		this.terraceArea = terraceArea;
	}
	public Double getGardenArea() {
		return gardenArea;
	}
	public void setGardenArea(Double gardenArea) {
		this.gardenArea = gardenArea;
	}
	public Double getPorchArea() {
		return porchArea;
	}
	public void setPorchArea(Double porchArea) {
		this.porchArea = porchArea;
	}
	public Double getOrangeryArea() {
		return orangeryArea;
	}
	public void setOrangeryArea(Double orangeryArea) {
		this.orangeryArea = orangeryArea;
	}
	public Boolean getIsFinished() {
		return isFinished;
	}
	public void setIsFinished(Boolean isFinished) {
		this.isFinished = isFinished;
	}
	public String getPdfUrl() {
		return pdfUrl;
	}
	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public List<ApartmentsApartmentAdditionalElements> getAdditionalElements() {
		return additionalElements;
	}
	public void setAdditionalElements(List<ApartmentsApartmentAdditionalElements> additionalElements) {
		this.additionalElements = additionalElements;
	}
	
	
	
}
